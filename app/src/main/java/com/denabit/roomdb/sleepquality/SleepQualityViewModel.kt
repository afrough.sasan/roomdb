package com.denabit.roomdb.sleepquality

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.denabit.roomdb.database.SleepDatabaseDao
import kotlinx.coroutines.*

class SleepQualityViewModel (
    val database: SleepDatabaseDao,
    private val sleepNightKey: Long = 0L) : ViewModel() {

    //region Vars
    private val viewModelJob = Job()
    private val uiScope = CoroutineScope(Dispatchers.Main + viewModelJob)

    private val _navigateToSleepTracker = MutableLiveData<Boolean?>()

    val navigateToSleepTracker: LiveData<Boolean?>
        get() = _navigateToSleepTracker
    //endregion

    fun doneNavigating(){
        _navigateToSleepTracker.value = null
    }

    override fun onCleared() {
        super.onCleared()
        viewModelJob.cancel()
    }

    fun onSetSleepQuality(quality:Int){
        uiScope.launch {
            withContext(Dispatchers.IO){
                val toNight = database.get(sleepNightKey) ?: return@withContext
                toNight.sleepQuality = quality
                database.update(toNight)
            }
            _navigateToSleepTracker.value = true
        }
    }



}