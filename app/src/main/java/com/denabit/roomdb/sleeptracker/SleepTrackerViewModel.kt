package com.denabit.roomdb.sleeptracker

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Transformations
import com.denabit.roomdb.database.SleepDatabaseDao
import com.denabit.roomdb.database.SleepNight
import com.denabit.roomdb.formatNights
import kotlinx.coroutines.*

class SleepTrackerViewModel(
    val database: SleepDatabaseDao,
    application: Application
) : AndroidViewModel(application) {

    var viewModelJOB = Job()

    private val uiScope = CoroutineScope(Dispatchers.Main + viewModelJOB)

    private var toNight = MutableLiveData<SleepNight?>()

    private val nights = database.getAllNights()

    val formattedNights = Transformations.map(nights){
            nights -> formatNights(nights, application.resources)
    }
    private val _navigateToSleepQuality = MutableLiveData<SleepNight>()

    val navigateToSleepQuality: LiveData<SleepNight>
        get() = _navigateToSleepQuality

    init {
        initializeToNight()
    }

    private fun initializeToNight() {
        uiScope.launch {
            toNight.value = getToNightFromDatabase()
        }
    }

    private suspend fun getToNightFromDatabase(): SleepNight? {

        return withContext(Dispatchers.IO) {
            var night = database.getTonight()
            if (night?.startTimeMilli != night?.endTimeMilli) {
                night = null
            }
            return@withContext night
        }

    }

     fun onStartTracking() {
        uiScope.launch {

            var night = SleepNight()
            insert(night)
            toNight.value = getToNightFromDatabase()

        }
    }
    val startButtonEnabled = Transformations.map(toNight){
        null == it
    }

    val stopButtonEnabled = Transformations.map(toNight){
        null != it
    }

    val clearButtonEnabled = Transformations.map(nights){
        it?.isNotEmpty()
    }
    private val _showSnackbarEvent = MutableLiveData<Boolean>()

    val showSnackbarEvent: LiveData<Boolean>
        get() = _showSnackbarEvent

    private suspend fun insert(night: SleepNight) {

        withContext(Dispatchers.IO) {
            database.insert(night)
        }

    }
    fun doneShowingSnackbar(){
        _showSnackbarEvent.value = false
    }
    fun doneNavigating(){
        _navigateToSleepQuality.value = null
    }
    fun onStopTracking() {
        uiScope.launch {
            val oldNight = toNight.value ?: return@launch
            oldNight.endTimeMilli = System.currentTimeMillis()
            update(oldNight)
            _navigateToSleepQuality.value = oldNight
        }
    }

    private suspend fun update(night: SleepNight) {
        withContext(Dispatchers.IO) {
            database.update(night)
        }
    }

    fun onClear() {
        uiScope.launch {
            clear()
            toNight.value = null
            _showSnackbarEvent.value = true
        }
    }

    private suspend fun clear() {
        withContext(Dispatchers.IO) {
            database.clear()
        }
    }

    override fun onCleared() {
        super.onCleared()
        viewModelJOB.cancel()
    }

}